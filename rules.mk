#----------------------------------------------------------------------------
# make gergo:germ:dfu
# Make sure you have dfu-programmer installed!
#----------------------------------------------------------------------------
# Firmware options
STENO_ENABLE						= yes
STENO_PROTOCOL						= geminipr
MOUSEKEY_ENABLE 					= no
PROGRAMMABLE_BUTTON_ENABLE				= yes
KEY_LOCK_ENABLE						= no
SWAP_HANDS_ENABLE					= no

GRAVE_ESC_ENABLE					= no
SPACE_CADET_ENABLE					= no
LTO_ENABLE						= yes
NKRO_ENABLE						= yes

#Debug options
VERBOSE 		 				= no
DEBUG_MATRIX_SCAN_RATE   				= no
DEBUG_MATRIX		 				= no
CONSOLE_ENABLE						= yes

#Combos!
VPATH               			+=  keyboards/gboards/

# A bunch of stuff that you shouldn't touch unless you
# know what you're doing.
#
# No touchy, capiche?
SRC += matrix.c i2c_master.c
ifeq ($(strip $(DEBUG_MATRIX)), yes)
    OPT_DEFS += -DDEBUG_MATRIX
endif
