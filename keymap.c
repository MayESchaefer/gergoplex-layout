#include QMK_KEYBOARD_H
#include "keymap_steno.h"
#include "process_leader.h"

#include "print.h"
void keyboard_post_init_user(void) {
  debug_enable=true;
}

enum layers {
	BASE,// main layer
	TH_FAKE_LAYER,
	TH_FAKE_LAYER_2,
	TH_FAKE_LAYER_3,
	LOCK,// locked main layer
	MODI,// altkeys
	PLOV,// plover
	DARK,// dark souls
	MINE,// minecraft
	RECI,// receiver
	SS13,// space station 13/14
	SS13_NUMALT,// ss13 numrow layer
	WASD,// wasd lock
	SWKS,// stormworks
	AROW,// arrows
	FKEY,// function keys
	MMOA,
	MODE// mode toggle layer
};

static const char *layerStrings[] = {
	"main layer",
	"FAKE_LAYER_1",
	"FAKE_LAYER_2",
	"FAKE_LAYER_3",
	"locked main layer",
	"altkeys layer",
	"plover layer",
	"dark souls game layer",
	"minecraft game layer",
	"receiver game layer",
	"space station 13/14 game layer",
	"ss13 numrow layer",
	"wasd lock layer",
	"stormworks game layer",
	"arrows layer",
	"function keys layer",
	"atlyss mmo game layer",
	"mode toggle layer"
};

enum my_keycodes {
	RSET = SAFE_RANGE,
	SMASH
};

//main layer tapholds
#define TH_Z_LEFT		LT(TH_FAKE_LAYER,	KC_Z)
#define TH_N_LEFT		LT(TH_FAKE_LAYER,	KC_N)
#define TH_X_DOWN		LT(TH_FAKE_LAYER,	KC_X)
#define TH_M_DOWN		LT(TH_FAKE_LAYER,	KC_M)
#define TH_C_UP			LT(TH_FAKE_LAYER,	KC_C)
#define TH_COMM_UP		LT(TH_FAKE_LAYER,	KC_COMM)
#define TH_V_RIGHT		LT(TH_FAKE_LAYER,	KC_V)
#define TH_DOT_RIGHT	LT(TH_FAKE_LAYER,	KC_DOT)
#define TH_R_ESC		LT(TH_FAKE_LAYER,	KC_R)
#define TH_T_TAB		LT(TH_FAKE_LAYER,	KC_T)
#define TH_I_QUOT		LT(TH_FAKE_LAYER,	KC_I)
#define TH_Y_DEL		LT(TH_FAKE_LAYER,	KC_Y)
#define TH_O_EQL		LT(TH_FAKE_LAYER,	KC_O)
#define TH_P_MINS		LT(TH_FAKE_LAYER,	KC_P)
//ss13 tapholds
#define TH_G_ESC		LT(TH_FAKE_LAYER_2,	KC_G)
#define TH_N_EQL		LT(TH_FAKE_LAYER_2,	KC_N)
#define TH_U_QUOT		LT(TH_FAKE_LAYER_2,	KC_U)
#define TH_J_MINS		LT(TH_FAKE_LAYER_2,	KC_J)
#define TH_M_P0			LT(TH_FAKE_LAYER_2,	KC_M)
#define TH_COMM_P1		LT(TH_FAKE_LAYER_2,	KC_COMM)
#define TH_DOT_P2		LT(TH_FAKE_LAYER_2,	KC_DOT)
#define TH_SLSH_P3		LT(TH_FAKE_LAYER_2,	KC_SLSH)
#define TH_K_P4			LT(TH_FAKE_LAYER_2,	KC_K)
#define TH_L_P5			LT(TH_FAKE_LAYER_2,	KC_L)
#define TH_SCLN_P6		LT(TH_FAKE_LAYER_2,	KC_SCLN)
#define TH_I_P7			LT(TH_FAKE_LAYER_2,	KC_I)
#define TH_O_P8			LT(TH_FAKE_LAYER_2,	KC_O)
#define TH_P_P9			LT(TH_FAKE_LAYER_2,	KC_P)
//stormworks tapholds
#define TH_U_0			LT(TH_FAKE_LAYER_3,	KC_U)
#define TH_I_7			LT(TH_FAKE_LAYER_3,	KC_I)
#define TH_O_8			LT(TH_FAKE_LAYER_3,	KC_O)
#define TH_K_4			LT(TH_FAKE_LAYER_3,	KC_K)
#define TH_L_5			LT(TH_FAKE_LAYER_3,	KC_L)
//layer tap toggles
#define LTG_SLSH_AROW	LT(TH_FAKE_LAYER,	KC_SLSH)

//taphold macro
#define TH_FUNC(l, kc_tap, kc_hold)		case LT(l,kc_tap): if (!record->tap.count) { if (record->event.pressed) { register_code(kc_hold); } else { unregister_code(kc_hold); } return false; } return true
//layer tap toggle macro
#define LTG_FUNC(fake_layer, kc_tap, real_layer)		case LT(fake_layer,kc_tap): if (!record->tap.count && record->event.pressed) { layer_invert(real_layer); return false; } return true

//#include "g/keymap_combo.h"

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	switch (keycode) {
		//main layer tapholds
		case LT(TH_FAKE_LAYER,KC_Z):
		TH_FUNC(TH_FAKE_LAYER, KC_N, KC_LEFT);
		case LT(TH_FAKE_LAYER,KC_X):
		TH_FUNC(TH_FAKE_LAYER, KC_M, KC_DOWN);
		case LT(TH_FAKE_LAYER,KC_C):
		TH_FUNC(TH_FAKE_LAYER, KC_COMM, KC_UP);
		case LT(TH_FAKE_LAYER,KC_V):
		TH_FUNC(TH_FAKE_LAYER, KC_DOT, KC_RIGHT);
		TH_FUNC(TH_FAKE_LAYER, KC_R, KC_ESC);
		TH_FUNC(TH_FAKE_LAYER, KC_T, KC_TAB);
		TH_FUNC(TH_FAKE_LAYER, KC_I, KC_QUOT);
		TH_FUNC(TH_FAKE_LAYER, KC_Y, KC_DEL);
		TH_FUNC(TH_FAKE_LAYER, KC_O, KC_EQL);
		TH_FUNC(TH_FAKE_LAYER, KC_P, KC_MINS);
		//ss13 tapholds
		TH_FUNC(TH_FAKE_LAYER_2, KC_G, KC_ESC);
		TH_FUNC(TH_FAKE_LAYER_2, KC_N, KC_EQL);
		TH_FUNC(TH_FAKE_LAYER_2, KC_U, KC_QUOT);
		TH_FUNC(TH_FAKE_LAYER_2, KC_J, KC_MINS);
		TH_FUNC(TH_FAKE_LAYER_2, KC_M, KC_P0);
		TH_FUNC(TH_FAKE_LAYER_2, KC_COMM, KC_P1);
		TH_FUNC(TH_FAKE_LAYER_2, KC_DOT, KC_P2);
		TH_FUNC(TH_FAKE_LAYER_2, KC_SLSH, KC_P3);
		TH_FUNC(TH_FAKE_LAYER_2, KC_K, KC_P4);
		TH_FUNC(TH_FAKE_LAYER_2, KC_L, KC_P5);
		TH_FUNC(TH_FAKE_LAYER_2, KC_SCLN, KC_P6);
		TH_FUNC(TH_FAKE_LAYER_2, KC_I, KC_P7);
		TH_FUNC(TH_FAKE_LAYER_2, KC_O, KC_P8);
		TH_FUNC(TH_FAKE_LAYER_2, KC_P, KC_P9);
		//stormworks tapholds
		TH_FUNC(TH_FAKE_LAYER_3, KC_U, KC_0);
		TH_FUNC(TH_FAKE_LAYER_3, KC_I, KC_7);
		TH_FUNC(TH_FAKE_LAYER_3, KC_O, KC_8);
		TH_FUNC(TH_FAKE_LAYER_3, KC_K, KC_4);
		TH_FUNC(TH_FAKE_LAYER_3, KC_L, KC_5);

		case KC_F6:
		case KC_F7:
		case KC_F8:
		case KC_F9:
		case KC_F10:
			if (get_mods() & MOD_MASK_SHIFT) {
				if (record->event.pressed) {
					switch (keycode) { // actually use key-specific effects
						case KC_F6: tap_code(KC_INS); break;
						case KC_F7: tap_code(KC_HOME); break;
						case KC_F8: tap_code(KC_END); break;
						case KC_F9: tap_code(KC_PGUP); break;
						case KC_F10: tap_code(KC_PGDN); break;
					}
				}
				return false;
			}
			else {
				return true;
			}
		case RSET:
			if (record->event.pressed) {
				layer_clear();
			}
			return false;
		LTG_FUNC(TH_FAKE_LAYER, KC_SLSH, AROW);
		case SMASH:
			if (record->event.pressed) {
				int numberOfChars = 10 + (rand() % 20);
				for (int i = 0; i < numberOfChars; i++) {
				    uint8_t key = rand() % 31;//set to 61 for whole keyboard keysmash
					switch (key) {
						case 0 ... 2: send_char('a'); break;
						case 3 ... 5: send_char('s'); break;
						case 6 ... 8: send_char('d'); break;
						case 9 ... 11: send_char('f'); break;
						case 12 ... 14: send_char('g'); break;
						case 15 ... 17: send_char('h'); break;
						case 18 ... 20: send_char('j'); break;
						case 21 ... 23: send_char('k'); break;
						case 24 ... 26: send_char('l'); break;
						case 27 ... 29: send_char(';'); break;
						case 30: send_char('\''); break;
						case 31 ... 56: send_char(key - 31 + 'a'); break;
						case 57: send_char('['); break;
						case 58: send_char('/'); break;
						case 59: send_char(','); break;
						case 60: send_char('.'); break;
					}
				}
			}
			return true;
		default:
			if ((keycode & 0xffe0) == QK_TOGGLE_LAYER && record->event.pressed) {//code is TG and key is pressed
				uint8_t layercode = keycode & 0x1F;
				bool layerActive = layer_state_is(layercode);
				if (layerActive) {
					uprintf("toggled %s off\n", layerStrings[layercode]);
				}
				else {
					uprintf("toggled %s on\n", layerStrings[layercode]);
				}
				return true;
			}
			return true; // Process all other keycodes normally
	}
}

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Main layer
 *
 * ,-----+-----+-----+-----+------.        ,------+------+-----+------+-----.
 * |  q  |  w  |  e  |r/ESC|t/tab |        |y/del |  u   | i/' | o/=  | p/- |
 * |-----+-----+-----+-----+------|        |------+------+-----+------+-----|
 * |  a  |s/alt|d/ctl|f/sft|g/MODI|        |h/MODI|j/shft|k/ctl|l/alt |  ;  |
 * |-----+-----+-----+-----+------|        |------+------+-----+------+-----|
 * |z/lft|x/dwn|c/up |v/rgt|b/FKEY|        |n/left|m/down|,/up |./rght|  /  |
 * `-----+-----+-----+-----+------'        `------+------+-----+------+-----'
 * ,------+-------+-----------.            ,------------+------+------.
 * | META | SPACE | CMPS/AROW |            | ENTER/AROW | BACK | MODE |
 * `------+-------+-----------'            `------------+------+------'
 */
[BASE] = LAYOUT_split_3x5_3(
	KC_Q,			KC_W,				KC_E,				TH_R_ESC,			TH_T_TAB,			/**/	TH_Y_DEL,		KC_U,				TH_I_QUOT,			TH_O_EQL,			TH_P_MINS,
	KC_A,			MT(MOD_LALT, KC_S),	MT(MOD_LCTL, KC_D),	MT(MOD_LSFT, KC_F),	LT(MODI, KC_G),		/**/	LT(MODI, KC_H),	MT(MOD_LSFT, KC_J),	MT(MOD_LCTL, KC_K),	MT(MOD_LALT, KC_L),	KC_SCLN,
	TH_Z_LEFT,		TH_X_DOWN,			TH_C_UP,			TH_V_RIGHT,			LT(FKEY, KC_B),		/**/	TH_N_LEFT,		TH_M_DOWN,			TH_COMM_UP,			TH_DOT_RIGHT,		KC_SLSH,

	KC_LGUI, KC_SPACE, LT(AROW, KC_SCRL),							/**/	LT(AROW, KC_ENTER), KC_BSPC, OSL(MODE)
),
[LOCK] = LAYOUT_split_3x5_3(
	KC_Q,	KC_W,	KC_E,	KC_R,	KC_T,		/**/	KC_Y,	KC_U,	KC_I,		KC_O,	KC_P,
	KC_A,	KC_S,	KC_D,	KC_F,	KC_G,		/**/	KC_H,	KC_J,	KC_K,		KC_L,	KC_SCLN,
	KC_Z,	KC_X,	KC_C,	KC_V,	KC_B,		/**/	KC_N,	KC_M,	KC_COMM,	KC_DOT,	KC_SLSH,

	_______, _______, _______,					/**/	_______, _______, _______
),
/* Alternate layer
 *
 * ,-----+-----+-----+-----+-----.      ,-----+-----+-----+-----+-----.
 * |  0  |  7  |  8  |  9  |     |      |     |     |     |     |     |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * |     |  4  |  5  |  6  |     |      |     |  [  |  ]  |  \  |  `  |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * |     |  1  |  2  |  3  |     |      |     |     |     |     |     |
 * `-----+-----+-----+-----+-----'      `-----+-----+-----+-----+-----'
 * ,-----+-----+-----.                  ,-----+-----+-----.
 * |     |     |     |                  |     |     |     |
 * `-----+-----+-----'                  `-----+-----+-----'
 */
[MODI] = LAYOUT_split_3x5_3(
	KC_0,		KC_7,	KC_8,	KC_9,	_______,	/**/	_______,	_______,	_______,	_______,	_______,
	_______,	KC_4,	KC_5,	KC_6,	_______,	/**/	_______,	KC_LBRC,	KC_RBRC,	KC_BSLS,	KC_GRV,
	_______,	KC_1,	KC_2,	KC_3,	_______,	/**/	_______,	_______,	_______,	_______,	_______,

	_______, _______, _______,						/**/	_______, _______, _______
),
/* Plover layer
 *
 * ,-----+-----+-----+-----+-----.      ,-----+-----+-----+-----+-----.
 * |  S- |  T- |  P- |  H- |  *  |      | -F  | -P  | -L  | -T  | -D  |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * |  s- |  K- |  W- |  R- |  *  |      | -R  | -B  | -G  | -S  | -Z  |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * | NUM | NUM | NUM | NUM | NUM |      | NUM | NUM | NUM | NUM | NUM |
 * `-----+-----+-----+-----+-----'      `-----+-----+-----+-----+-----'
 * ,-----+-----+-------.                ,-------+-----+------.
 * |     |  A  |   O   |                |   E   |  U  | EXIT |
 * `-----+-----+-------'                `-------+-----+------'
 */
[PLOV] = LAYOUT_split_3x5_3(
	STN_S1,	STN_TL,	STN_PL,	STN_HL,	STN_ST1,	/**/	STN_FR,	STN_PR,	STN_LR,	STN_TR,	STN_DR,
	STN_S2,	STN_KL,	STN_WL,	STN_RL,	STN_ST2,	/**/	STN_RR,	STN_BR,	STN_GR,	STN_SR,	STN_ZR,
	STN_N1,	STN_N2,	STN_N3,	STN_N4,	STN_N5,		/**/	STN_N6,	STN_N7,	STN_N8,	STN_N9,	STN_NA,

	STN_FN,	STN_A,	STN_O,						/**/	STN_E,	STN_U,	TG(PLOV)
),
[DARK] = LAYOUT_split_3x5_3(
	KC_Q,		KC_W,		KC_E,	KC_R,	KC_T,		/**/	KC_Y,		KC_U,		KC_I,		KC_O,		KC_P,
	KC_A,		KC_S,		KC_D,	KC_F,	KC_G,		/**/	KC_H,		KC_J,		KC_K,		KC_L,		KC_TAB,
	KC_LCTL,	KC_LSFT,	KC_C,	KC_V,	KC_LALT,	/**/	KC_ESC,		KC_LEFT,	KC_DOWN,	KC_UP,		KC_RIGHT,

	_______, _______, KC_LCTL,							/**/	_______, _______, _______
),
[MINE] = LAYOUT_split_3x5_3(
	KC_Q,		KC_W,		KC_E,	KC_R,	KC_T,		/**/	KC_TAB,		KC_0,	KC_7,	KC_8,	KC_9,
	KC_A,		KC_S,		KC_D,	KC_F,	KC_G,		/**/	KC_H,		KC_J,	KC_4,	KC_5,	KC_6,
	KC_LCTL,	KC_LSFT,	KC_C,	KC_V,	KC_LALT,	/**/	KC_ESC,		KC_M,	KC_1,	KC_2,	KC_3,

	_______, _______, KC_LCTL,							/**/	_______, _______, _______
),
[RECI] = LAYOUT_split_3x5_3(
	KC_GRV,		KC_Q,	KC_W,	KC_E,	KC_R,			/**/	KC_T,		KC_0,	KC_7,	KC_8,	KC_9,
	KC_LSFT,	KC_A,	KC_S,	KC_D,	KC_F,			/**/	KC_H,		KC_J,	KC_4,	KC_5,	KC_6,
	KC_LCTL,	KC_Z,	KC_C,	KC_V,	KC_G,			/**/	KC_ESC,		KC_M,	KC_1,	KC_2,	KC_3,

	_______, _______, KC_T,								/**/	_______, _______, _______
),
[SS13] = LAYOUT_split_3x5_3(
	KC_Q,		KC_W,		KC_E,					MT(MOD_LCTL, KC_R),	TH_T_TAB,	/**/	TH_Y_DEL,				TH_U_QUOT,	TH_I_P7,	TH_O_P8,	TH_P_P9,
	KC_A,		KC_S,		KC_D,					MT(MOD_LSFT, KC_F),	TH_G_ESC,	/**/	LT(SS13_NUMALT,KC_H),	TH_J_MINS,	TH_K_P4,	TH_L_P5,	TH_SCLN_P6,
	KC_Z,		KC_X,		LT(SS13_NUMALT,KC_C),	MT(MOD_LALT, KC_V),	KC_B,		/**/	TH_N_EQL,				TH_M_P0,	TH_COMM_P1,	TH_DOT_P2,	TH_SLSH_P3,

	_______, _______, _______,														/**/	_______, _______, _______
),
[SS13_NUMALT] = LAYOUT_split_3x5_3(
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	_______,	KC_7,	KC_8,	KC_9,
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	_______,	KC_4,	KC_5,	KC_6,
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	KC_0,		KC_1,	KC_2,	KC_3,

	_______,	_______,	_______,							/**/	_______,	_______,	_______
),
[SWKS] = LAYOUT_split_3x5_3(
	KC_Q,		KC_W,		KC_E,	KC_R,	KC_T,		/**/	KC_TAB,		TH_U_0,	TH_I_7,	TH_O_8,	KC_9,
	KC_A,		KC_S,		KC_D,	KC_F,	KC_G,		/**/	KC_H,		KC_J,	TH_K_4,	TH_L_5,	KC_6,
	KC_LCTL,	KC_LSFT,	KC_C,	KC_V,	KC_LALT,	/**/	KC_ESC,		KC_M,	KC_1,	KC_2,	KC_3,

	_______, _______, KC_LCTL,							/**/	_______, _______, _______
),
[MMOA] = LAYOUT_split_3x5_3(
	KC_Q,					KC_W,					KC_E,	KC_R,	KC_T,		/**/	KC_TAB,		KC_U,	KC_I,	KC_O,	KC_P,
	KC_A,					KC_S,					KC_D,	KC_F,	KC_G,		/**/	KC_6,		KC_7,	KC_8,	KC_9,	KC_0,
	MT(MOD_LCTL, KC_1),		MT(MOD_LSFT, KC_2),		KC_3,	KC_4,	KC_5,		/**/	KC_ESC,		KC_F1,	KC_F2,	KC_F3,	KC_F4,

	_______, _______, KC_LSFT,													/**/	_______, _______, _______
),
[WASD] = LAYOUT_split_3x5_3(
	_______,	KC_W,		_______,	_______,	_______,	/**/	_______,	_______,	_______,	_______,	_______,
	KC_A,		KC_S,		KC_D,		_______,	_______,	/**/	_______,	_______,	_______,	_______,	_______,
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	_______,	_______,	_______,	_______,

	_______,	_______,	_______,							/**/	_______,	_______,	_______
),
[AROW] = LAYOUT_split_3x5_3(
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	_______,	_______,	_______,	_______,
	KC_LEFT,	KC_DOWN,	KC_UP,		KC_RGHT,	_______,	/**/	_______,	KC_LEFT,	KC_DOWN,	KC_UP,		KC_RGHT,
	_______,	_______,	_______,	_______,	_______,	/**/	_______,	_______,	_______,	_______,	_______,

	_______,	_______,	_______,							/**/	_______,	_______,	_______
),
/* F-key layer
 *
 * ,-----+-----+-----+-----+-----.      ,-----+-----+-----+-----+-----.
 * | F7  | F8  | F9  | F12 |     |      |     | F7  | F8  | F9  | F12 |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * | F4  | F5  | F6  | F11 |     |      |     | F4  | F5  | F6  | F11 |
 * |-----+-----+-----+-----+-----|      |-----+-----+-----+-----+-----|
 * | F1  | F2  | F3  | F10 |     |      |     | F1  | F2  | F3  | F10 |
 * `-----+-----+-----+-----+-----'      `-----+-----+-----+-----+-----'
 * ,-----+-----+-----.                  ,-----+-----+-----.
 * |     |     |     |                  |     |     |     |
 * `-----+-----+-----'                  `-----+-----+-----'
 */
[FKEY] = LAYOUT_split_3x5_3(
	KC_F7,	KC_F8,	KC_F9,	KC_F12,	_______,		/**/	_______,	_______,	_______,	_______,	_______,
	KC_F4,	KC_F5,	KC_F6,	KC_F11,	_______,		/**/	_______,	_______,	_______,	_______,	_______,
	KC_F1,	KC_F2,	KC_F3,	KC_F10,	_______,		/**/	_______,	_______,	_______,	_______,	_______,

	_______, _______, _______,						/**/	_______, _______, _______
),
/* Mode layer
 *
 * ,------+------+------+------+------.      ,-------+------+------+------+------.
 * | ESC  | PREV | PLAY | NEXT | XXX  |      | RESET | NUM  | XXX  | LOCK | MINE |
 * |------+------+------+------+------|      |-------+------+------+------+------|
 * | TAB  | VOLU | XXX  | CLR  | MODI |      |  MODI | PLOV | XXX  | SWKS | DARK |
 * |------+------+------+------+------|      |-------+------+------+------+------|
 * | XXX  | VOLD | XXX  | XXX  | FKEY |      |  RECI | XXX  | SMSH | SS13 | AROW |
 * `------+------+------+------+------'      `-------+------+------+------+------'
 * ,-----+-----+-----.                       ,-----+-----+-----.
 * | XXX | XXX | XXX |                       | XXX | XXX |     |
 * `-----+-----+-----'                       `-----+-----+-----'
 */
[MODE] = LAYOUT_split_3x5_3(
	KC_ESC,	KC_MPRV,	KC_MPLY,	KC_MNXT,	KC_NO,		/**/	RSET,		KC_NUM,		KC_NO,		TG(LOCK),	TG(MINE),
	KC_TAB,	KC_VOLU,	TG(WASD),	EE_CLR,		TG(MODI),	/**/	TG(MODI),	TG(PLOV),	TG(MMOA),	TG(SWKS),	TG(DARK),
	KC_NO,	KC_VOLD,	KC_NO,		KC_NO,		TG(FKEY),	/**/	TG(RECI),	KC_NO,		SMASH,		TG(SS13),	TG(AROW),

	XXXXXXX, XXXXXXX, XXXXXXX,								/**/	XXXXXXX, XXXXXXX, _______
)
};
